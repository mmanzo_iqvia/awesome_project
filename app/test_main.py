from app import main

def test_div():
    assert main.div_func(15, 5) == 3
    assert main.div_func(15, 0) == 0